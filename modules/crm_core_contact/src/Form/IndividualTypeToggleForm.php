<?php

namespace Drupal\crm_core_contact\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Form for enable/disable individual types.
 */
class IndividualTypeToggleForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    $args = array(
      '%toggle' => $this->getRequest()->get('op'),
      '%type' => $this->getEntity()->label(),
    );

    $question = '';

    switch ($this->getRequest()->get('op')) {
      case 'enable':
        $question = $this->t('Are you sure you want to enable the individual type %type?', $args);
        break;

      case 'disable':
        $question = $this->t('Are you sure you want to disable the individual type %type?', $args);
        break;
    }

    return $question;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('When a individual type is disabled, you cannot add any individuals to this individual type. You will also not be able to search for individuals of disabled individual type.');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    switch ($this->getRequest()->get('op')) {
      case 'disable':
        $text = $this->t('Disable');
        break;

      default:
      case 'enable':
        $text = $this->t('Enable');
        break;
    }

    return $text;
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.crm_core_individual_type.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    switch ($this->getRequest()->get('op')) {

      case 'disable':
        $action = $this->t('disabled');
        $this->entity->disable()->save();
        break;

      default:
      case 'enable':
        $action = $this->t('enabled');
        $this->entity->enable()->save();
        break;
    }
    $t_args = array(
      '%name' => $this->entity->label(),
      '%toggle' => $action,
    );
    drupal_set_message($this->t('The individual type %name has been %toggle.', $t_args));

    $form_state->setRedirect('entity.crm_core_individual_type.collection');
  }

}
