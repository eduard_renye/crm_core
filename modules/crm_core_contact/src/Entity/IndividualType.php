<?php

namespace Drupal\crm_core_contact\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\Core\Entity\EntityDescriptionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\crm_core_contact\ContactTypeInterface;

/**
 * CRM Individual Type Entity Class.
 *
 * @ConfigEntityType(
 *   id = "crm_core_individual_type",
 *   label = @Translation("CRM Core Individual type"),
 *   bundle_of = "crm_core_individual",
 *   config_prefix = "type",
 *   handlers = {
 *     "access" = "Drupal\crm_core_contact\IndividualTypeAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\crm_core_contact\Form\IndividualTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *       "toggle" = "Drupal\crm_core_contact\Form\IndividualTypeToggleForm",
 *     },
 *     "list_builder" = "Drupal\crm_core_contact\ContactTypeListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer individual types",
 *   entity_keys = {
 *     "id" = "type",
 *     "label" = "name",
 *     "status" = "disabled",
 *   },
 *   config_export = {
 *     "name",
 *     "type",
 *     "description",
 *     "locked",
 *     "primary_fields",
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/crm-core/individual-types/add",
 *     "edit-form" = "/admin/structure/crm-core/individual-types/{crm_core_individual_type}",
 *     "delete-form" = "/admin/structure/crm-core/individual-types/{crm_core_individual_type}/delete",
 *     "enable" = "/admin/structure/crm-core/individual-types/{crm_core_individual_type}/enable",
 *     "disable" = "/admin/structure/crm-core/individual-types/{crm_core_individual_type}/disable",
 *   }
 * )
 */
class IndividualType extends ConfigEntityBundleBase implements ContactTypeInterface, EntityDescriptionInterface {

  /**
   * The machine-readable name of this type.
   *
   * @var string
   */
  public $type;

  /**
   * The human-readable name of this type.
   *
   * @var string
   */
  public $name;

  /**
   * A brief description of this type.
   *
   * @var string
   */
  public $description;

  /**
   * Whether or not this type is locked.
   *
   * A boolean indicating whether this type is locked or not, locked individual
   * type cannot be edited or disabled/deleted.
   *
   * @var boolean
   */
  public $locked;

  /**
   * Primary fields.
   *
   * An array of key-value pairs, where key is the primary field type and value
   * is real field name used for this type.
   *
   * @var array
   */
  public $primary_fields;

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->type;
  }

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage, array &$values) {
    parent::preCreate($storage, $values);

    // Ensure default values are set.
    $values += array(
      'locked' => FALSE,
    );
  }

  /**
   * {@inheritdoc}
   *
   * @todo This does not scale.
   *
   * Deleting a individual type with thousands of individual records associated
   * will run into execution timeout.
   */
  public static function preDelete(EntityStorageInterface $storage, array $entities) {
    parent::preDelete($storage, $entities);

    $ids = array_map(function(EntityInterface $entity) {
      return $entity->id();
    }, $entities);

    // Delete all instances of the given type.
    $results = \Drupal::entityQuery('crm_core_individual')
      ->condition('type', $ids, 'IN')
      ->execute();

    if (!empty($results)) {
      $individuals = Individual::loadMultiple($results);
      \Drupal::entityTypeManager()->getStorage('crm_core_individual')->delete($individuals);
      \Drupal::logger('crm_core_individual')->info('Delete @count individual due to deletion of individual type.', ['@count' => count($results)]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function loadActive() {
    $ids = \Drupal::entityQuery('crm_core_individual')
      ->condition('status', TRUE)
      ->execute();

    return IndividualType::loadMultiple($ids);
  }

  /**
   * {@inheritdoc}
   */
  public static function getNames() {
    $individual_types = IndividualType::loadMultiple();
    $individual_types = array_map(function ($individual_type) {
      return $individual_type->label();
    }, $individual_types);
    return $individual_types;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

}
